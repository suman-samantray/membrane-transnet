import os

"""
The following section adds BetaVersion as module to python library.
Path has to be changed to directory, that includes the folder 'BetaVersion'.

User Input Needed:

"""

import sys
path = '/home/alex/Desktop' # change
sys.path.append(path)
import BetaVersion

################################################
#                                              #
#       Give path to protein trajectory        #
#                                              #
################################################

topfile = 'out-protein-last500frame.pdb' # change
trajfile = 'out-protein_skip50.xtc' # change
number_of_molecules = 2
number_of_frames = 11 # change

"""
Not anymore.
"""

topology = os.path.join(os.getcwd(), topfile)
trajectory = os.path.join(os.getcwd(), trajfile)

"""
Computation
"""
v = True
if (v == True):
    c = BetaVersion.Compute(Trajectory=trajectory, Topology=topology, Number_of_Molecules=number_of_molecules, Number_of_Frames=number_of_frames, Write_hdf5=True, Chunksize=1001)
    c.Oligomersize(Cutoff=0.3) # change
    c.Saltbridge_Contacts(Cutoff=0.8) # change
    c.Hydrophobic_Contacts(Cutoff=0.3) # change
    c.Compactness() # change

"""
Getting results
"""
r = BetaVersion.Reader(number_of_molecules, number_of_frames)
f1 = r.Oligomersize()
f2 = r.Saltbridge_Contacts()
f3 = r.Hydrophobic_Contacts()
f4 = r.Compactness()

"""
Membrane feature
"""

import mdtraj
import numpy as np

################################################
#                                              #
#   Give path to protein + lipid trajectory    #
#                                              #
################################################

pdb = '/home/alex/Desktop/Hiba/for-ALEX/out-protein-lipid-last500frame.pdb' # change
xtc = '/home/alex/Desktop/Hiba/for-ALEX/out-protein-lipid_skip50.xtc' # change

num_mol = 2

top = mdtraj.load(pdb).topology
traj = mdtraj.load(xtc, top=top)

table, bonds = top.to_dataframe()
res = np.zeros(top.n_atoms)
resnumber = 0
resname_start = table['resSeq'][0]
for atom_index, resname in enumerate(table['resSeq']):
    if resname == resname_start:
        res[atom_index] = int(resnumber)

    else:
        resname_start = table['resSeq'][atom_index]
        resnumber += 1
        res[atom_index] = int(resnumber)

protein_atoms = top.select('protein and name CA')
chainA_atoms, chainB_atoms = np.split(protein_atoms, num_mol)
membrane_atoms = top.select('name P')

possible_contacts = []
for chain in [chainA_atoms, chainB_atoms]:
    chain_membrane_pair = []
    for protein_atom in chain:
        for membrane_atom in membrane_atoms:
            chain_membrane_pair.append([res[protein_atom], res[membrane_atom]])
    possible_contacts.append(chain_membrane_pair)

Contacts = np.zeros((number_of_frames, num_mol), dtype=int)
for idx, traj_frame in enumerate(traj):
    # print(idx)
    for chain, theo_cont in enumerate(possible_contacts):
        output = mdtraj.compute_contacts(traj_frame, theo_cont)

        distances = output[0][0]
        residue_pairs = output[1]

        Cutoff = 0.8 # change

        for pair, d in zip(residue_pairs, distances):
            if (d < Cutoff):
                Contacts[idx][chain] = 1
                break

np.save('F5.npy', Contacts)
f5 = np.load('F5.npy')

# states = r.Extract_States(f1, f2, f3, f4, f5)
# for frame, state in enumerate(states):
#     print(15*'-', 'Frame', frame, 15*'-')
#     for s in state:
#         print(s, end=' ')
#     print('\n')

r.Network_Data(f1, f2, f3, f4, f5, Min_Population=0.0, Gexf_Name="Network.gexf") # change
