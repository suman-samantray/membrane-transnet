import numpy as np
import mdtraj

pdb = 'out-lipid-protein.pdb'
# xtc = 'out-lipid-protein.xtc'

xtc = 'short.xtc'

num_mol = 2

top = mdtraj.load(pdb).topology
traj = mdtraj.load(xtc, top=top)

table, bonds = top.to_dataframe()
res = np.zeros(top.n_atoms)
resnumber = 0
resname_start = table['resSeq'][0]
for atom_index, resname in enumerate(table['resSeq']):
    if resname == resname_start:
        res[atom_index] = int(resnumber)

    else:
        resname_start = table['resSeq'][atom_index]
        resnumber += 1a
        res[atom_index] = int(resnumber)

protein_atoms = top.select('protein and name CA')
chainA_atoms, chainB_atoms = np.split(protein_atoms, num_mol)
membrane_atoms = top.select('name P')

possible_contacts = []
for chain in [chainA_atoms, chainB_atoms]:
    chain_membrane_pair = []
    for protein_atom in chain:
        for membrane_atom in membrane_atoms:
            chain_membrane_pair.append([res[protein_atom], res[membrane_atom]])
    possible_contacts.append(chain_membrane_pair)

# print(np.shape(possible_contacts))
Contacts = []
for i in range(2):
    output = mdtraj.compute_contacts(traj, possible_contacts[i])

    distances = output[0]
    residue_pairs = output[1]

    residues_in_contact = set()
    contacts = []

    Cutoff = 0.5
    idx = 0
    cont = 0
    for frame in distances:
        for pair, d in zip(residue_pairs, frame):
            if (d < Cutoff):
#                print(i, pair)
                cont += 1
        contacts.append(cont)
        cont = 0
        idx += 1

    contacts = np.array(contacts)
#    print(np.where(contacts >= 1))
#    print(residues_in_contact)
    Contacts.append(contacts)
#    print(np.where(res == 507), np.where(res == 465))
#    print(np.where(res == 505), np.where(res == 107))

print(np.array(Contacts).T)

# f5 = np.zeros(traj.n_frames)
# for idx, (i, j) in enumerate(zip(Contacts[0], Contacts[1])):
#     if (i > 0):
#         if (j > 0):
#             f5[idx] = 2
#         else:
#             f5[idx] = 1
#     elif (j > 0):
#         f5[idx] = -1
#     else:
#         f5[idx] = 0
#
