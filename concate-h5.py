import sys
path = '/media/alex/NASB/Forschungen/Revoltec/Masterthesis/TN' # change
sys.path.append(path)
import BetaVersion

number_of_molecules = 2

"""
Generate concatenated Network.
"""

hdf5s = ['1.hdf5', '2.hdf5', '3.hdf5', '4.hdf5',
         '5.hdf5', '6.hdf5', '7.hdf5', '8.hdf5',
         '9.hdf5']

npys = ['1.npy', '2.npy', '3.npy', '4.npy',
         '5.npy', '6.npy', '7.npy', '8.npy',
         '9.npy']

number_of_frames = [1, 2, 3, 4, 5, 6, 7, 8, 9]

f1 = [] # olig global
f2 = [] # hydro global
f3 = [] # membr global

for hdf5_file, npy_file, frames in zip(hdf5s, npys, number_of_frames):
    r = BetaVersion.Reader(number_of_molecules, frames, Filename=hdf5_file)

    f1.append(r.Oligomersize())
    f2.append(r.Hydrophobic_Contacts())
    f3.append(np.load(npy_file))

speak = False
if speak == True:
    states = r.Extract_States(f1, f2, f3)
    for frame, state in enumerate(states):
        print(15*'-', 'Frame', frame+1, 15*'-')
        for s in state:
            print(s, end=' ')
        print('\n')

r.Network_Data(f1, f2, f3, Min_Population=0.1, Gexf_Name="Network_concatenated.gexf")
